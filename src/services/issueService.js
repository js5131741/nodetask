// services/issueService.js
const axios = require("axios");
const Issue = require("../models/issueModel");
const { githubToken } = require("../utils/constants");

async function getIssue(issueId) {
  const issueFromDB = await Issue.findOne({ number: issueId });
  return issueFromDB;
}

async function updateIssue(issueId, updatedIssueDetails) {
  const updatedIssue = await Issue.findOneAndUpdate(
    { number: issueId },
    updatedIssueDetails,
    { new: true }
  );

  if (!updatedIssue) {
    throw new Error("Issue not found");
  }

  // Update issue in GitHub
  try {
    const { data: githubResponse } = await axios.patch(
      `${apiurl}/issues/${issueId}`,
      updatedIssueDetails,
      {
        headers: {
          Authorization: `Bearer ${githubToken}`,
        },
      }
    );

    console.log(`GitHub response for issue ${issueId} update:`, githubResponse);
  } catch (error) {
    console.error(`Error updating issue ${issueId} on GitHub:`, error.message);
    throw new Error("Internal server error");
  }
}

module.exports = {
  getIssue,
  updateIssue,
};
