const axios = require("axios");
const Issue = require("../models/issueModel");
const { githubToken, githubApiUrl } = require("../utils/constants");

async function syncIssues() {
  const githubToken = "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";
  const githubApiUrl = "https://api.github.com/repos/Rangaraj99/demo";

  try {
    // Open issues
    const { data: openIssues } = await axios.get(`${githubApiUrl}/issues`, {
      headers: {
        Authorization: `Bearer ${githubToken}`,
      },
    });

    // Closed issues
    const { data: closedIssues } = await axios.get(
      `${githubApiUrl}/issues?state=closed`,
      {
        headers: {
          Authorization: `Bearer ${githubToken}`,
        },
      }
    );
    const issues = [...openIssues, ...closedIssues];

    // Batch API requests
    for (let i = 0; i < issues.length; i += 3) {
      const batch = issues.slice(i, i + 3);
      await Promise.all(
        batch.map(async (issue) => {
          try {
            const validatedIssue = new Issue(issue);
            await validatedIssue.validate();
          } catch (error) {
            console.error(
              `Validation error for issue ${issue.number}: ${error.message}`
            );
            return;
          }

          try {
            const updatedIssue = await Issue.findOneAndUpdate(
              { number: issue.number },
              issue,
              { upsert: true, new: true }
            );
            console.log(
              `Issue ${issue.number} synced successfully:`,
              updatedIssue
            );
          } catch (error) {
            console.error(
              `Error updating issue ${issue.number}: ${error.message}`
            );
          }
        })
      );

      // Wait for 1 second between batches
      await new Promise((resolve) => setTimeout(resolve, 1000));
    }
  } catch (error) {
    console.error("Error during issue synchronization:", error.message);
    throw error; // Propagate the error to the calling function
  }
}

module.exports = {
  syncIssues,
};
