// controllers/issueController.js
const issueService = require("../services/issueService");
const {
  validateUpdateRequestBody,
} = require("../middleware/validationMiddleware");

async function getIssue(req, res) {
  try {
    const { issue_id } = req.params;

    const issueFromDB = await issueService.getIssue(issue_id);

    if (!issueFromDB) {
      return res
        .status(404)
        .json({ message: "Issue not found in the database" });
    }

    res.json(issueFromDB);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
}

async function updateIssue(req, res) {
  try {
    const { issue_id } = req.params;
    const updatedIssueDetails = req.body;
    validateUpdateRequestBody(req, res, () => {});
    await issueService.updateIssue(issue_id, updatedIssueDetails);

    res.json({ message: "Issue details updated successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
}

module.exports = {
  getIssue,
  updateIssue,
};
