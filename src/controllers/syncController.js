// controllers/syncController.js
const axios = require("axios");
const syncService = require("../services/syncService");
const {
  validateSyncRequestBody,
} = require("../middleware/validationMiddleware");

async function syncIssues(req, res) {
  try {
    validateSyncRequestBody(req, res, () => {});
    await syncService.syncIssues();
    res.json({ message: "Sync completed successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
}

module.exports = {
  syncIssues,
};
