const Joi = require("joi");

const validateRequestBody = (schema) => {
  return (req, res, next) => {
    const { error } = schema.validate(req.body);
    if (error) {
      return res.status(400).json({ error: error.details[0].message });
    }
    next();
  };
};

const syncSchema = Joi.object({
  key: Joi.string().required(),
});

const updateSchema = Joi.object({
  title: Joi.string(),
  body: Joi.string(),
  state: Joi.string(),
  number: Joi.number(),
});

module.exports = {
  validateSyncRequestBody: validateRequestBody(syncSchema),
  validateUpdateRequestBody: validateRequestBody(updateSchema),
};
