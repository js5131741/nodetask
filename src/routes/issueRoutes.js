const express = require("express");
const { getIssue, updateIssue } = require("../controllers/issueController");

const router = express.Router();

router.get("/issues/:issue_id", async (req, res) => {
  const result = await getIssue(req.params.issue_id);
  res.json(result);
});

router.put("/issues/:issue_id", async (req, res) => {
  const result = await updateIssue(req.params.issue_id, req.body);
  res.json(result);
});

module.exports = router;
