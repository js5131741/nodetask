const express = require("express");
const { syncIssues } = require("../controllers/syncController");

const router = express.Router();

router.post("/sync", async (req, res) => {
  const result = await syncIssues();
  res.json(result);
});

module.exports = router;
