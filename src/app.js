const express = require("express");
const mongoose = require("mongoose");
const syncController = require("./controllers/syncController");
const issueController = require("./controllers/issueController");
const {
  
  jsonParser,
} = require("./middleware/notFoundMiddleware");
const { json } = require("sequelize");

const app = express();
const PORT = 3000;

app.use(express.json());

mongoose.connect("mongodb://localhost:27017/github-issues-sync", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
const db = mongoose.connection;

db.on("error", console.error.bind(console, "MongoDB connection error:"));
db.once("open", () => {
  console.log("Connected to MongoDB");
});

// Sync API
app.post("/sync", syncController.syncIssues);

// Get Issue
app.get("/issues/:issue_id", issueController.getIssue);

// Update Issue
app.put("/issues/:issue_id", issueController.updateIssue);
app.use(jsonParser);
app.use(notFoundMiddleware);

// Server
app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});
