// middleware/notFoundMiddleware.js
const express = require("express");
const { json } = express;


const jsonParser = json();

module.exports = {
  jsonParser,
};
