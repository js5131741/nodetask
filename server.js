const express = require("express");
const mongoose = require("mongoose");
const axios = require("axios");

const app = express();
const PORT = 3000;

app.use(express.json());

// MongoDB Connection
mongoose.connect("mongodb://localhost:27017/github-issues-sync", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
const db = mongoose.connection;

db.on("error", console.error.bind(console, "MongoDB connection error:"));
db.once("open", () => {
  console.log("Connected to MongoDB");
});

const token = "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";
const apiurl = "https://api.github.com/repos/Rangaraj99/demo";

// Schema
const issueSchema = new mongoose.Schema(
  {
    title: { type: String, required: true },
    body: { type: String, required: false },
    number: { type: Number, required: true },
    state: { type: String, required: true },
  },
  { collection: "issues" },
  { timestamps: true }
);

const Issue = mongoose.model("Issue", issueSchema);

// Sync API
app.post("/sync", async (req, res) => {
  try {
    // Open issues
    const { data: openIssues } = await axios.get(`${apiurl}/issues`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    // Closed issues
    const { data: closedIssues } = await axios.get(
      `${apiurl}/issues?state=closed`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const issues = [...openIssues, ...closedIssues];

    // Batch API requests
    for (let i = 0; i < issues.length; i += 3) {
      const batch = issues.slice(i, i + 3);
      await Promise.all(
        batch.map(async (issue) => {
          try {
            const validatedIssue = new Issue(issue);
            await validatedIssue.validate();
          } catch (error) {
            console.error(
              `Validation error for issue ${issue.number}: ${error.message}`
            );
            return;
          }

          try {
            const updatedIssue = await Issue.findOneAndUpdate(
              { number: issue.number },
              issue,
              { upsert: true, new: true }
            );
            console.log(
              `Issue ${issue.number} synced successfully:`,
              updatedIssue
            );
          } catch (error) {
            console.error(
              `Error updating issue ${issue.number}: ${error.message}`
            );
          }
        })
      );

      // Wait for 1 second between batches
      await new Promise((resolve) => setTimeout(resolve, 1000));
    }

    res.json({ message: "Sync completed successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
});

// Get Issue
app.get("/issues/:issue_id", async (req, res) => {
  try {
    const { issue_id } = req.params;

    const issueFromDB = await Issue.findOne({ number: issue_id });

    if (!issueFromDB) {
      return res
        .status(404)
        .json({ message: "Issue not found in the database" });
    }

    res.json(issueFromDB);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
});

// Update Issue
app.put("/issues/:issue_id", async (req, res) => {
  try {
    const { issue_id } = req.params;
    const updatedIssueDetails = req.body;

    // Update issue in MongoDB
    try {
      const updatedIssue = await Issue.findOneAndUpdate(
        { number: issue_id },
        updatedIssueDetails,
        { new: true }
      );

      if (!updatedIssue) {
        return res.status(404).json({ message: "Issue not found" });
      }

      console.log(`Issue ${issue_id} updated in MongoDB:`, updatedIssue);
    } catch (error) {
      console.error(
        `Error updating issue ${issue_id} in MongoDB:`,
        error.message
      );
      return res.status(500).json({ message: "Internal server error" });
    }

    // Update issue in GitHub
    try {
      const { data: githubResponse } = await axios.patch(
        `${apiurl}/issues/${issue_id}`,
        updatedIssueDetails,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );

      console.log(
        `GitHub response for issue ${issue_id} update:`,
        githubResponse
      );
    } catch (error) {
      console.error(
        `Error updating issue ${issue_id} on GitHub:`,
        error.message
      );
      return res.status(500).json({ message: "Internal server error" });
    }

    res.json({ message: "Issue details updated successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
});

// Server
app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});
